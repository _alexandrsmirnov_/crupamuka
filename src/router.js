import Vue from 'vue'
import Router from 'vue-router'
import mainPage from './components/mainPage.vue'
import createRequestPaid from './components/createRequestForms/createRequestPaid'
import createRequestProduct from './components/createRequestForms/createRequestProduct'
import createRequestUploadPhoto from './components/createRequestForms/createRequestUploadPhoto'
import createRequestDelivery from './components/createRequestForms/createRequestDelivery'
import searchPageFirst from './components/searchPages/searchPageFirst'




Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'mainPage',
            component: mainPage
        },
        {
            path: '/create-request-paid',
            name: 'createRequestPaid',
            component: createRequestPaid
        },
        {
            path: '/create-request-product',
            name: 'createRequestProduct',
            component: createRequestProduct
        },
        {
            path: '/create-request-upload-photo',
            name: 'createRequestUploadPhoto',
            component: createRequestUploadPhoto
        },
        {
            path: '/create-request-delivery',
            name: 'createRequestDelivery',
            component: createRequestDelivery
        },
        {
            path: '/search-page-first',
            name: 'searchPageFirst',
            component: searchPageFirst
        },
    ]
})
